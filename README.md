# README #

## 実行方法
1. [ここ](https://sites.google.com/a/chromium.org/chromedriver/home) からChromeDriverをダウンロードする
1. zipファイルを解凍し、このディレクトリ直下に移動する
1. `pip3 install -r requirements.txt`で必要なライブラリをインストールする
1. `python3 main.py`で実行する

## 検索対象について
* 対象は、csv/school_org_list.csv にある学校法人となっている