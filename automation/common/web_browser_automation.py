""" Webブラウザの自動化 """
from typing import Dict
from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver


class WebBrowserAutomation:
    """ WebBrowserAutomationクラス """

    def __init__(self, path: str, url: str) -> None:
        """ コンストラクタ
        """
        self.__url = url
        self.__driver = webdriver.Chrome(executable_path=path)
        self.__driver.get(url)

    @property
    def driver(self) -> WebDriver:
        """ WebDriverを返す
        """
        return self.__driver

    def back(self) -> None:
        """ ブラウザバックする
        """
        self.driver.back()

    def move_to(self, url: str = "") -> None:
        """ URLに遷移
        """
        if url == "":
            url = self.__url
        self.driver.get(self.__url)

    def element(self, xpaths: Dict[str, str]) -> None:
        """ Elementを取得する
        """
        for k, v in xpaths.items():
            exec("self.{} = self.driver.find_element_by_xpath('{}')".format(k, v))
