""" 学校法人一覧のWebブラウザ自動化 """
import os

from selenium.webdriver.remote.webelement import WebElement

from school_org.automation.common import WebBrowserAutomation

XPATHS = {
    "radio_kanji": "/html/body/center/form[4]/table[2]/tbody/tr[2]/td[4]/input",
    "text_box": "/html/body/center/form[4]/table[2]/tbody/tr[8]/td/input",
    "submit_button": "/html/body/center/form[4]/table[3]/tbody/tr/td/input[1]",
}


class SchoolOrg(WebBrowserAutomation):
    """ SchoolOrgクラス """
    text_box: WebElement
    submit_button: WebElement

    def __init__(self) -> None:
        """ コンストラクタ
        """
        url = "https://meibo.shigaku.go.jp/top"
        path = os.path.abspath("chromedriver")
        super().__init__(path, url)

    def switch_kanji(self) -> None:
        """ 法人情報検索を漢字に切り替える
        """
        self.driver.find_element_by_xpath(XPATHS["radio_kanji"]).click()

    def input_text(self, value: str) -> None:
        """ テキストボックスに入力する
        """
        self.driver.find_element_by_xpath(XPATHS["text_box"]).send_keys(value)

    def click_submit(self) -> None:
        """ 検索ボタンを押下
        """
        self.driver.find_element_by_xpath(XPATHS["submit_button"]).click()

    def clear_text_box(self) -> None:
        """ 検索ボックスを消す
        """
        self.driver.find_element_by_xpath(XPATHS["text_box"]).clear()

    def search(self, name: str) -> None:
        """ 検索する
        Args:
            name: 検索する名称
        """
        self.input_text(name)
        self.click_submit()
