""" 実行ファイル """
import csv
import os
import time
import signal

from school_org.automation.school_org import SchoolOrg


def main():
    """ mainメソッド
    """
    school_org = None
    try:
        school_org = SchoolOrg()
        school_org.switch_kanji()
        with open(os.path.abspath("csv") + "/school_org_list.csv") as f:
            orgs = csv.reader(f)
            for org in orgs:
                school_org.search(org[0])
                time.sleep(5)
                school_org.back()
                school_org.clear_text_box()
    finally:
        os.kill(school_org.driver.service.process.pid, signal.SIGTERM)


if __name__ == '__main__':
    main()
